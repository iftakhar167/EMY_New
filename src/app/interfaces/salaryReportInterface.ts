export interface SalaryReport {
  name: string;
  bsalary: number;
  allownsOne: number;
  allownsTwo: number;
  monthTotal: number;
  dailyRate: number;
  ratePerHour: number;
  present: number;
  absent: number;
  workkingsDays: number;
  salaryOfMonth: number;
  overtimeHoure: number;
  overtimeAmount: number;
  bounsHour: number;
  bounsHourAmount: number;
  grossTotal: number;
}
